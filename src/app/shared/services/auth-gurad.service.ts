import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';


/* admin before login */
@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuradLogin implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let role = sessionStorage.getItem("role");
    if(role == 'admin') {
      this.router.navigate(["/admin-dashboard"]);
      return false;
    }
    else{
      return true;
    }
  }

}

/* admin after login */
@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuradService{

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let role = sessionStorage.getItem("role");
    if(role == 'admin') {
      return true;
    }
    else{
      this.router.navigate(["/admin-login"]);
      return false;
    }
  }

}

/* customer (buyer & seller) befor login check*/
@Injectable({
  providedIn: 'root'
})
export class SellerBuyerAuthGuardLogin implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let role = sessionStorage.getItem("role");
    if(role == 'seller') {
      this.router.navigate(["/seller-dashboard"]);
      return false;
    }
    else if(role == 'buyer'){
      this.router.navigate(["/buyer-dashboard"]);
      return false;
    }
    else{
      return true;
    }
  }

}

/* buyer after login */
@Injectable({
  providedIn: 'root'
})
export class BuyerAuthGuradService {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let role = sessionStorage.getItem("role");
    if(role == 'buyer') {
      return true;
    }
    else{
      this.router.navigate(["/sign-in"]);
      return false;
    }
  }

}

/* seller after login */
@Injectable({
  providedIn: 'root'
})
export class SellerAuthGuradService {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let role = sessionStorage.getItem("role");
    if(role == 'seller') {
      return true;
    }
    else{
      this.router.navigate(["/sign-in"]);
      return false;
    }
  }

}
